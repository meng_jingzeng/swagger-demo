package com.meng.swagger.controller;

import com.meng.swagger.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "控制器")//对控制层进行说明
@RestController
public class HelController {

    @ApiOperation("hello控制类")//此注解给接口进行说明
    @GetMapping("/hello")
    public String hel(){
        return "sdsd";
    }
    //只要接口返回值存在实体类，就会被扫描到swagger的model中，但是属性必须是public才会显示，private则会隐藏
    // 可以在实体类上注解@ApiModel("用户实体类")来说明该类作用
    //也可在实体类的某个属性注解@ApiModelProperty("用户名")表示该字段的作用
    @PostMapping("user")
    public User user(String username){
        return new User();
    }
    @ApiOperation("post测试")
    @PostMapping("post")
    public User postt(@ApiParam("用户") User user){
        return new User();
    }
}
