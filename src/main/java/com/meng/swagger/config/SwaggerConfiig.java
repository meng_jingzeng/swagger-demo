package com.meng.swagger.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2 //开启
public class SwaggerConfiig {

    //swagger的bean实例
    @Bean
    public Docket docket(Environment environment){
        //设置要显示的swagger环境，项目中一般有发布前的开发坏境，发布后生产环境，配置文件aproperties多样
        Profiles profiles = Profiles.of("dev","test");  //获得apropeties文件，参数可以是多个
        //通过environment.acceptsProfiles获取项目环境判断是否处在自己设定的环境当中
        boolean flag = environment.acceptsProfiles(profiles);


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("meng")  //api分组
                //true表示打开swagger,false表示关闭,不写enable默认true，flag是上面Profiles.of获取项目环境看处于哪个阶段判读是否打开swagger，是dev配置文件则打开
                //多个aproperties文件中如application.properties、application-dev.properties、application-pro.properties
                //当application.properties中spring.profiles.active=dev，则指定激活application-dev.properties，则flag为true，这种好处是在配置文件就可以决定是否打开swagger
                .enable(flag)
                .select()
                //RequestHandlerSelectors.basePackage("com.meng.swagger.controller")配置要扫描的接口的方式，只会扫描该包下的接口,此为常用
                //.any() 表示全部扫描
                //.none()表示都不扫描
                //.withClassAnnotation(RestController.class)表示扫描类上有@RestController注解的类上接口
                //.withMethodAnnotation(GetMappering)扫描GetMappering方法接口
                .apis(RequestHandlerSelectors.basePackage("com.meng.swagger.controller"))
                //过滤,只扫描meng下面的接口
//                .paths(PathSelectors.ant("/meng/**"))
                .build();
    }
    //多人开发会有多个Bean实例,即分组开发接口
    @Bean
    public Docket docket1(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("张三"); //其余配置同上
    }
    @Bean
    public Docket docket2(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("李四");
    }



    //接口的提示信息，项目说明等
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("污水厂项目接口")//大标题
                .description("有点酷")//详细描述
                .version("2.0")//版本
                .termsOfServiceUrl("https://www.cnblogs.com/Meng2113/")
                .contact(new Contact("蒙京增", "https://www.cnblogs.com/Meng2113/", "764730326@qq.com"))//作者
                .license("The Apache License, Version 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }
}
